﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace geminerd
{
    class DataHandler
    {
        public class BTC
        {
            [Key]
            public DateTime Time { get; set; }
            public double Price { get; set; }
        }
        public class MyContext : DbContext
        {
            public DbSet<BTC> BTCs { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                string root = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                root = Path.Combine(root, "geminerd.db");
                optionsBuilder.UseSqlite(string.Format("Data Source='{0}';", root));
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<BTC>().ToTable("BTC");
            }
        }
    }
}

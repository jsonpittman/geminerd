﻿using System;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using System.Collections.Generic;
using TimeZoneConverter;
using System.Linq;
using static geminerd.DataHandler;

namespace geminerd
{
        class Program
    {
        static void Main(string[] args)
        {
            WebClient client = new WebClient();
            DataHandler.MyContext ents = new DataHandler.MyContext();
            TimeZoneInfo easternZone = TZConvert.GetTimeZoneInfo("Eastern Standard Time");

            // add latest price to db
            const string service_url = "https://api.coindesk.com/v1/bpi/currentprice.json";
            var json = client.DownloadString(service_url);
            dynamic jsonDe = JsonConvert.DeserializeObject(json);
            string time = jsonDe["time"]["updated"].Value;
            time = time.Substring(0, time.LastIndexOf(' '));
            var rate = double.Parse(jsonDe["bpi"]["USD"]["rate"].Value);

            BTC b = new BTC();
            b.Price = rate;
            b.Time = DateTime.Parse(time);
            if(ents.BTCs.Where(w => w.Time == b.Time).Count() == 0) {
                ents.BTCs.Add(b);
                ents.SaveChanges();
            }

            // write gem pages
            Gem.WritePage("index");
            Gem.WritePage("coin");
        }
    }
}

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using TimeZoneConverter;
using static geminerd.DataHandler;

namespace geminerd
{
    internal class Gem
    {
        public static void WritePage(string name)
        {
            string root = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            string source = Path.Combine(root, "gemini", name + ".gmi");
            string dest = Path.Combine(root, "output", name + ".gmi");
            string logo = Path.Combine(root, "templates", "logo.txt");
            var index = File.ReadAllText(source);
            index = index.Replace("[logo]", File.ReadAllText(logo));

            if (index.Contains("[chart_hour]"))
            {
                DataHandler.MyContext ents = new DataHandler.MyContext();
                BTC curr = ents.BTCs.OrderByDescending(o => o.Time).FirstOrDefault();
                index = index.Replace("[current]", curr.Price.ToString("C", CultureInfo.CurrentCulture));
                TimeZoneInfo easternZone = TZConvert.GetTimeZoneInfo("Eastern Standard Time");
                index = index.Replace("[update]", TimeZoneInfo.ConvertTimeFromUtc(curr.Time, easternZone).ToString());

                List<double> series = new List<double>();
                string[] sp = new string[] { "", "" };
                DateTime easternTime = DateTime.Now;

                index = index.Replace("[chart_hour]", AsciiChart.Sharp.AsciiChart.Plot(
                    ents.BTCs.Where(e => e.Time >= easternTime)
                        .OrderByDescending(o => o.Time)
                        .Take(60)
                        .OrderBy(o => o.Time)
                        .Select(s => s.Price)
                        .ToList(),
                    new AsciiChart.Sharp.Options
                    {
                        Height = 15,
                        AxisLabelFormat = "00,000",
                        AxisLabelLeftMargin = 0,
                        AxisLabelRightMargin = 1
                    }
                ));

                var chart_24 = ents.BTCs.Where(e => e.Time.Minute == 0 || e.Time.Minute == 20 || e.Time.Minute == 40)
                    .OrderByDescending(o => o.Time)
                    .Take(60)
                    .OrderBy(o => o.Time)
                    .Select(s => s.Price)
                    .ToList();

                if (chart_24.Count == 0)
                    index = index.Replace("[chart_24]", "- No Data -");
                else
                    index = index.Replace("[chart_24]", AsciiChart.Sharp.AsciiChart.Plot(chart_24,
                        new AsciiChart.Sharp.Options
                        {
                            Height = 15,
                            AxisLabelFormat = "00,000",
                            AxisLabelLeftMargin = 0,
                            AxisLabelRightMargin = 1
                        }
                    ));

                var url = "https://api.coindesk.com/v1/bpi/historical/close.json";
                var json = new WebClient().DownloadString(url);
                dynamic jsonDe = JsonConvert.DeserializeObject(json);
                foreach (var price in jsonDe["bpi"])
                {
                    series.Add((double)price.Value);
                }
                index = index.Replace("[chart_30d]", AsciiChart.Sharp.AsciiChart.Plot(series.ToArray(),
                    new AsciiChart.Sharp.Options
                    {
                        Height = 15,
                        AxisLabelFormat = "00,000",
                        AxisLabelLeftMargin = 0,
                        AxisLabelRightMargin = 1
                    }
                ));
            }

            File.WriteAllText(dest, index, Encoding.UTF8);
        }
    }
}